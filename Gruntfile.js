module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Development server
    json_server: {
      options: {
        port: 13337,
        hostname: '0.0.0.0',
        db: 'src/api/address.json'
      },
      your_target: ['src/api/address.json', 'src/api/delivery.json', 'src/api/paymnet.json']
    }
  });

  grunt.loadNpmTasks('grunt-json-server');

  grunt.registerTask('default', ['json_server']);
};
