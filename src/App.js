import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { AppProvider } from './AppContext';
import Header from './components/Header';
import Footer from './components/Footer';
import ModalBackdrop from './components/ModalBackdrop';
import Welcome from './pages/Welcome';
import SignIn from './pages/SignIn';
import Delivery from './pages/Delivery';
import Payment from './pages/Payment';
import Styleguide from './pages/Styleguide';

class App extends Component {
  render() {
    return (
      <AppProvider>
        <Router>
          <div className="app-container">
            <Header />
            <ModalBackdrop />
            <div className="app-content">
              <Switch>
                <Route exact path="/" component={Welcome} />
                <Route exact path="/signin" component={SignIn} />
                <Route exact path="/delivery" component={Delivery} />
                <Route exact path="/payment" component={Payment} />
                <Route exact path="/styleguide" component={Styleguide} />
              </Switch>
            </div>

            <Footer />
          </div>
        </Router>
      </AppProvider>
    );
  }
}

export default App;
