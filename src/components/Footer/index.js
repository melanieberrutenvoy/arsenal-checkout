import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="footer__section text-center">
                <div className="footer__section__icon footer__section__icon--large glyphicon glyphicon-comment" />
                <div className="footer__section__text">Need help with your order?</div>

                <button className="footer__section__btn btn btn-ghost-primary"> Email us</button>
              </div>
              <hr className="footer__separator mb20 mt20 hidden-xs hidden-sm" />
            </div>
            <div className="col-xs-12 hidden-md hidden-lg">
              <hr className="footer__separator mb20 mt20" />
              <p className="text-center">
                Arsenal Direct, Highbury House, 75 <br /> Drayton Park, London, N5 1BU
              </p>
            </div>
          </div>
        </div>

        <div className="footer__sub hidden-md hidden-lg">
          <div className="container">
            <div className="row">
              <span className="text-center col-xs-12">ID:A0704622</span>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="footer__links col-md-12 hidden-xs hidden-sm">
              <div className="col-md-4">
                <ul className="footer__list list-inline">
                  <li>
                    <NavLink className="footer__links__link" to="/send-feedback">
                      Send feedback
                    </NavLink>
                  </li>

                  <li>
                    <NavLink className="footer__links__link" to="/basket">
                      Back to basket
                    </NavLink>
                  </li>
                </ul>
              </div>
              <div className="col-md-4">
                <div className="footer__section footer__section--inline text-center">
                  <div className="footer__section__icon glyphicon glyphicon-lock" />
                  <div className="footer__section__text footer__section__text--sub">
                    Arsenal store is a &nbsp;
                    <NavLink className="footer__section__link" to="/basket">
                      secure checkout
                    </NavLink>
                  </div>
                </div>
              </div>
              <div className="col-md-4 text-right">
                <ul className="footer__list list-inline">
                  <li>
                    <img
                      src="https://arsenal-uk.local:9002/_ui/responsive/common/images/payment_logos/amex.png"
                      alt="AMEX"
                    />
                  </li>

                  <li>
                    <img
                      src="https://arsenal-uk.local:9002/_ui/responsive/common/images/payment_logos/amex.png"
                      alt="AMEX"
                    />
                  </li>

                  <li>
                    <img
                      src="https://arsenal-uk.local:9002/_ui/responsive/common/images/payment_logos/amex.png"
                      alt="AMEX"
                    />
                  </li>

                  <li>
                    <img
                      src="https://arsenal-uk.local:9002/_ui/responsive/common/images/payment_logos/amex.png"
                      alt="AMEX"
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
