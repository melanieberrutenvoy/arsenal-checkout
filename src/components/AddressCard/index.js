import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import { Link } from 'react-router-dom';


class AddressCard extends Component {
  constructor() {
    super();

    this.handleEditAddress = this.handleEditAddress.bind(this);
    this.handleAddAddress = this.handleAddAddress.bind(this);
  }

  handleAddAddress() {
    // Tell storage that the address no longer exist
    this.props.context.updateDeliveryAddress('', true);
    localStorage.removeItem('addressSelected');
    // this.props.history.push('/delivery#2');
    this.props.history.replace();
  }
  handleEditAddress() {
    // Tell storage that the address no longer exist
    this.props.context.updateDeliveryAddress('', true);
    localStorage.removeItem('addressSelected');
    // this.props.history.push('/delivery#2');
    this.props.history.replace();
  }
  render() {
    let address = this.props.context.state.deliveryAddress;
    return (
      <div className="address-card">
        <div className="text-right">
          <Link
            to="/delivery#2"
            className="add-new-address btn btn-secondary btn-small"
            onClick={this.handleAddAddress}
          >
            Add new address
          </Link>
        </div>
        <div className="mb20 mt20">
          <Link to="/delivery#2" onClick={this.handleEditAddress} className="pull-right btn-link">
            Edit
          </Link>
          <div>{address.length > 0 && address.map((field, i) => <p key={i}>{field}</p>)}</div>
        </div>
      </div>
    );
  }
}

export default props => (
  <AppConsumer>{context => <AddressCard {...props} context={context} />}</AppConsumer>
);
