import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

class Map extends Component {
  constructor() {
    super();
    this.state = {
      map: null
    }

    this.mapLoaded = this.mapLoaded.bind(this);
  }

  mapLoaded() { //mapLoaded(map) {
    // if(this.state.map != null) {
    //   return 
    //   this.setState({map:map});
    // }
  }


  render() {
    // const markers = this.props.markers || [];
    return (
      <GoogleMap
        ref={this.mapLoaded}
        center={this.props.dynamicCenter}
        zoom={this.props.dynamicZoom}
        defaultZoom={this.props.zoom}
        defaultCenter={this.props.center}>
        {this.props.isMarkerShown && <Marker position={this.props.dynamicCenter} />}
      </GoogleMap>
    )
  }
};

export default withGoogleMap(Map);