import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Header extends Component {
	clearLocalStorage() {
		localStorage.clear();
	}

	render() {
		return (
			<React.Fragment>
				<div className="header">
					<div className="header__inner">
						<div className="container">
							<div className="header__holder row">
								<div className="col-xs-4 col-md-2">
									<div className="header__section text-center">
										<div className="header__section__icon glyphicon glyphicon-lock" />
										<div className="header__section__text">Secure checkout</div>
									</div>
								</div>
								<div className="col-md-2 hidden-xs hidden-sm">
									<div className="header__section text-center">
										<div className="header__section__icon glyphicon glyphicon-ok" />
										<div className="header__section__text">Official Arsenal Mechandise</div>
									</div>
								</div>
								<div className="col-xs-4 col-md-4">
									<div className="header__section header__section--logo text-center">
										<NavLink to="/">
											<img
												className="header__section__logo"
												alt="Arsenal Logo"
												src="https://arsenal-uk.local:9002/_ui/responsive/common/images/arsenal-crest.png"
											/>
										</NavLink>
									</div>
								</div>
								<div className="col-md-2 hidden-xs hidden-sm">
									<div className="header__section text-center">
										<div className="header__section__icon glyphicon glyphicon-comment" />
										<div className="header__section__text">Get help with your order</div>
									</div>
								</div>
								<div className="col-xs-4 col-md-2">
									<div className="header__section text-center">
										<div className="header__section__icon glyphicon glyphicon-earphone" />
										<div className="header__section__text">0345 1718 6123</div>
									</div>
								</div>
							</div>
							<button
								className="btn-small"
								onClick={this.clearLocalStorage.bind(this)}
								style={{ position: 'absolute', top: '0', zIndex: '10' }}
							>
								clear
							</button>
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Header;
