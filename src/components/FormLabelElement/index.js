import React, { Component } from 'react';

class FormLabelElement extends Component {
  render() {
    let element = this.props;

    if (element.selectOptions) {
      return (
        <div className="form-group">
          <label htmlFor={element.id} className="form-group__label">
            {element.label}
          </label>
          <div className="form-group__input">
            <select
              type="text"
              className="form-control "
              id={element.id}
              name={element.id}
              onChange={this.props.handleChange(this)} 
              data-validation={element.validationType}
              placeholder=""
            >
              <option value="">Please select</option>
              {element.selectOptions.map((option, i) => (
                <option key={i} value={option.value} disabled={option.disabled}>
                  {option.label}
                </option>
              ))}
            </select>
          </div>
        </div>
      );
    } else {
      return (
        <div className="form-group">
          <label htmlFor={element.id} className="form-group__label">
            {element.label}
          </label>
          <div className="form-group__input">
            <input
              type="text"
              className="form-control "
              id={element.id}
              name={element.id} 
              data-validation={element.validationType}
              onChange={this.props.handleChange(this)}
              placeholder=""
            />
          </div>
        </div>
      );
    }
  }
}

export default FormLabelElement;
