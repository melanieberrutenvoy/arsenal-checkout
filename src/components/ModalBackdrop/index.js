import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';

class ModalBackdrop extends Component {

	render() {
		const isVisible = this.props.context.state.modalOpened;
		return (
			<div className={isVisible ? 'modal-backdrop fade in' : ''}></div>
		);
	}
}

export default props => <AppConsumer>{context => <ModalBackdrop {...props} context={context} />}</AppConsumer>;