import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import AddressCard from '../AddressCard';
import NominatedDay from '../NominatedDay';

class DeliverySubType extends Component {

	constructor() {
		super();

		this.state = {
			deliveryOption: ''
		}

		this.handleDeliveryMethodChildSelected = this.handleDeliveryMethodChildSelected.bind(this);
		this.handleDeliveryInstructions = this.handleDeliveryInstructions.bind(this);
		this.validateDeliveryOption = this.validateDeliveryOption.bind(this);
	}

	componentWillMount() {
		// Delivery type
		let deliveryMethodChildSelected = JSON.parse(localStorage.getItem('deliveryMethodChildSelected')) || '';
		this.setState({ deliveryOption: deliveryMethodChildSelected });
	}

	handleDeliveryMethodChildSelected(e, key) {
    // Update the state with the delivery option selected
    let deliveryMethodChildSelected = e.target.id;
    this.setState({ deliveryOption: deliveryMethodChildSelected });
  }

  handleDeliveryInstructions(e) {
    this.setState({ deliveryInstructions: e.target.value });
  }

  validateDeliveryOption() {
    // Store the delivery option selected (in state)
    this.props.context.updateDeliveryMethodChildSelected(this.state.deliveryOption, true);
    localStorage.setItem('deliveryMethodChildSelected', JSON.stringify(this.state.deliveryOption));

    // Store delivery instructions
    this.props.context.updateDeliveryInstructions(this.state.deliveryInstructions, true);
    localStorage.setItem('deliveryInstructions', JSON.stringify(this.state.deliveryInstructions));
    this.props.history.push('/payment');
  }

	render() {
		let context = this.props.context.state;
		return (
			<React.Fragment>
				<h1 className="checkout-heading checkout-heading--no-border">Delivery to</h1>
				<AddressCard history={this.props.history} />
				<hr />
				<h1 className="checkout-heading checkout-heading--no-border">Choose a delivery option</h1>
				<div className="detailed-delivery-method">
					{context.deliveryMethodChild.map((type, i) => (
						<div key={i} className="form-group form-group--radio-selection form-group--list">
							<div className="radio-selection">
								<label
									htmlFor={type.id}
									className={
										this.state.deliveryOption === type.id
											? 'radio-selection__label radio-selection__label--active'
											: 'radio-selection__label'
									}
								>
									{type.label}
								</label>
								<input
									className="radio-selection__radio"
									type="radio"
									name={type.label}
									id={type.id}
									value={type.label}
									checked={this.state.deliveryOption === type.id}
									onChange={this.handleDeliveryMethodChildSelected}
								/>
								<ul className="radio-selection__list list-unstyled">
									<li className="radio-selection__item">
										{type.duration}
										<span className="pull-right">
											<strong>{type.price}</strong>
										</span>
									</li>
								</ul>
							</div>
						</div>
					))}
				</div>
				{this.state.deliveryOption === 'nominated' && <NominatedDay />}
				<div className="other-info col-xs-12">
					<div className="row">
						<div className="form-group">
							<label htmlFor="deliveryInstructions" className="form-group__label">
								Additional delivery instructions (optional)
							</label>
							<div className="form-group__input">
								<input
									type="text"
									className="form-control "
									id="deliveryInstructions"
									name="deliveryInstructions"
									onChange={this.handleDeliveryInstructions}
									placeholder=""
								/>
							</div>
						</div>
					</div>
				</div>
				<button
					className="btn btn-primary btn-block mb20"
					onClick={this.validateDeliveryOption}
					disabled={context.deliveryMethodParentSelected.length === 0}
				>
					Continue to payment
				</button>
			</React.Fragment>
		);
	}
}

export default props => <AppConsumer>{context => <DeliverySubType {...props} context={context} />}</AppConsumer>;
