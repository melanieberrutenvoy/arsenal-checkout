import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import StoreListItem from '../StoreListItem';
import Map from '../Map';
import StoreModal from '../StoreModal';

import endpoints from '../../endpoints';
import axios from 'axios';

class ClickCollect extends Component {
	constructor() {
		super();
		this.state = {
			selectedLat: '',
			selectedLng: '',
			selectedZoom: '',
			selectedStore: '',
			modalStore: '',
			tabView: 'list',
			collectionConfirmedStore: '',
			collectionPostcodeSelected: '',
			storesCount: '',
		};
		this.toggleTabs = this.toggleTabs.bind(this);
		this.viewStoreDetails = this.viewStoreDetails.bind(this);
		this.selectStore = this.selectStore.bind(this);
		this.searchCollectStores = this.searchCollectStores.bind(this);

	}

	componentWillMount() {
		let collectionConfirmedStore = JSON.parse(localStorage.getItem('collectionConfirmedStore')) || '';
    	this.props.context.updateConfirmedSelectedStore(collectionConfirmedStore, true);

    	// Click Collect postcode
	    let collectionPostcodeSelected = JSON.parse(localStorage.getItem('collectionPostcodeSelected')) || '';
	    this.props.context.updateCollectionStores(collectionPostcodeSelected, '', true);
	    this.setState({ collectionPostcodeSelected });
	}

	showOnMap(lat, lng, zoom, name) {
		this.setState({
			selectedLat: lat,
			selectedLng: lng,
			selectedZoom: zoom,
			selectedStore: name
		});
	}

	selectStore(storeName) {
		console.log("storeName", storeName);
		this.props.context.updateConfirmedSelectedStore(storeName, true);
		localStorage.setItem('collectionConfirmedStore', JSON.stringify(storeName));
	}

	toggleTabs(view) {
		this.setState({tabView: view});
	}

	viewStoreDetails(store) {
		this.setState({modalStore: store, isModalActive: true});
		this.props.context.updateModalVisibility(true, true);
	}

	searchCollectStores() {
	    axios
	      .get(endpoints.collectionPointOfServices)
	      .then(response => {
	    	this.setState({ storesCount: response.data.length });
	        this.setState({ collectionPostcodeSelected: this.refs.collectToStorePC.value, collectionPointOfServices: response.data });
	        this.props.context.updateCollectionStores(this.refs.collectToStorePC.value, this.state.collectionPointOfServices, true);
	        localStorage.setItem('collectionPointOfServices', JSON.stringify(this.state.collectionPointOfServices));
	        localStorage.setItem('collectionPostcodeSelected', JSON.stringify(this.refs.collectToStorePC.value));
	      })
	      .catch(function(error) {
	        console.log(error);
	      });
	  }

	render() {
		let context = this.props.context.state;
		let stores = context.collectionPointOfServices;

		return (
			<div className="click-collect">
				{context.collectionConfirmedStore.length === 0 && 
					<React.Fragment>
						<h1 className="checkout-heading">Where do you want to collect from</h1>

						<div className="form-group search-store">
	                      <label htmlFor="collectToStorePC" className="form-group__label search-store__label">
	                        Find your nearest collection points (enter valid postcode)
	                      </label>
	                      <div className="input-group">
	                        <input
	                          ref="collectToStorePC"
	                          type="text"
	                          className="form-control search-store__input"
	                          id="collectToStorePC"
	                          name="collectToStorePC"
	                          defaultValue={context.collectionPostcodeSelected}
	                          placeholder="Enter your postcode"
	                        />
	                        <span className="input-group-btn">
	                          <button className="btn btn-primary search-store__btn" onClick={this.searchCollectStores}>
	                            Search
	                          </button>
	                        </span>
	                      </div>
	                    </div>
                    </React.Fragment>
                }
				
				{context.collectionPointOfServices && context.collectionPointOfServices.length > 0 && 
					<React.Fragment>
						<StoreModal isActive={this.props.context.state.modalOpened} selectStore={(storeName) => this.selectStore(storeName)} store={this.state.modalStore} />
						<div className="click-collect-results">
							<div className="click-collect-results__header">
								<div className="row">
									<div className="col-xs-12 col-md-12">
										<span className="click-collect-results__summary">
											<span>{this.state.storesCount} results for '{this.state.collectionPostcodeSelected}'</span>
										</span>
									</div>
									<div className="col-xs-12 col-md-12 mt10">
										<ul className="nav nav-tabs click-collect-results__tabs" id="tabsView" role="tablist">
											<li className="nav-tabs__tab" role="presentation">
												<a onClick={()=>this.toggleTabs('map')} aria-controls="mapview" data-map-zoom="13" data-toggle="tab" role="tab">
													Map view
												</a>
											</li>
											<li className="active nav-tabs__tab" role="presentation">
												<a onClick={()=>this.toggleTabs('list')} aria-controls="listview" data-toggle="tab" role="tab">
													List view
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div className="tab-content">
								{this.state.tabView === 'list' && 
								<div className="tab-pane active jsListView" id="listview" role="tabpanel">
									<div>
										<div className="col-xs-12 col-md-12 click-collect-address">
											{stores.length > 0 && stores.map((store, i) => 
												<StoreListItem viewStoreDetails={() => this.viewStoreDetails(store)} selectStore={(storeName) => this.selectStore(storeName)} store={store} key={i} />
											)}
										</div>
									</div>
								</div>
								}
								{this.state.tabView === 'map' && 
								<div className="tab-pane jsMapView" id="mapview" role="tabpanel">
									<div className="row">
										<div className="col-xs-12">
											<div className="gm-infowindow" id="gm-infowindow" />
											<div className="click-collect-results__map" id="map-canvas">
												<Map
													isMarkerShown="true"
													dynamicCenter={{ lat: this.state.selectedLat, lng: this.state.selectedLng }}
													dynamicZoom={this.state.selectedZoom || 8}
													dynamicMarker={{ lat: this.state.selectedLat, lng: this.state.selectedLng }}
													center={{ lat: 48.8588376, lng: 2.2768488 }}
													zoom={8}
													containerElement={<div style={{ width: '100%', height: '100vh' }} />}
													mapElement={<div style={{ width: '100%', height: '100%' }} />}
												/>
											</div>
										</div>
									</div>
								</div>
								}
							</div>
						</div>
					</React.Fragment>
				}				
			</div>
		);
	}
}

export default props => <AppConsumer>{context => <ClickCollect {...props} context={context} />}</AppConsumer>;
