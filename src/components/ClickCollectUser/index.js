import React, { Component } from 'react';
import FormLabelElement from '../FormLabelElement';
import { AppConsumer } from '../../AppContext';
import endpoints from '../../endpoints';
import { validateNumber, validateAlphanumeric, validateNotEmpty } from '../../validate';
import axios from 'axios';

class ClickCollectUser extends Component {
  constructor() {
    super();

    this.state = {
      title: '',
      firstName: '',
      lastName: '',
      mobile: '',
      titles: [],
      errors: {
        title: true,
        firstName: true,
        lastName: true,
        mobile: true
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.confirmCollectUserDetails = this.confirmCollectUserDetails.bind(this);
    
  }

  componentDidMount() {

    axios.get(endpoints.userDetails).then(response => {
      let res = response.data;
      let result = Object.keys(res).map(k => res[k]);
      this.setState(prevState => ({ titles: result }));
    });

    // Collector
    let collectionCollectorDetails = JSON.parse(localStorage.getItem('collectionCollectorDetails')) || '';
    this.props.context.updatecollectionCollectorDetails(collectionCollectorDetails, true);
    console.log("this.props.context", this.props.context);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
    let value = evt.target.value;
    let validation = evt.target.attributes.getNamedItem('data-validation').value;
    let result;

    if(validation === 'number') {
      result = validateNumber(value);
    } else if(validation === 'alphanumeric') {
      result = validateAlphanumeric(value);
    } else if(validation === 'notEmpty') {
      result = validateNotEmpty(value);
    }

    this.setState({
      errors: { ...this.state.errors, [evt.target.name]: !result },
    });
  }

  confirmCollectUserDetails() {
    let collectionCollectorDetails = [{title: this.state.title, firstName: this.state.firstName, lastName: this.state.lastName, mobile: this.state.mobile}];
    this.props.context.updatecollectionCollectorDetails(collectionCollectorDetails, true);
    localStorage.setItem('collectionCollectorDetails', JSON.stringify(collectionCollectorDetails));

    this.props.history.push('/payment');
  }

  render() {
    let hasAnyErrors =
      this.state.errors.title ||
      this.state.errors.firstName ||
      this.state.errors.lastName ||
      this.state.errors.mobile;

    return (
      <div className="address-block--collection">
        <div className="clearfix">
          <FormLabelElement
            handleChange={() => this.handleChange}
            selectOptions={this.state.titles}
            validationType="notEmpty"
            label="Title"
            id="title"
          />
          <FormLabelElement
            handleChange={() => this.handleChange}
            validationType="alphanumeric"
            label="Firstname"
            id="firstName"
          />
          <FormLabelElement
            handleChange={() => this.handleChange}
            validationType="alphanumeric"
            label="Lastname"
            id="lastName"
          />
          <FormLabelElement
            handleChange={() => this.handleChange}
            validationType="number"
            label="Mobile"
            id="mobile"
          />
        </div>
        <div className="clearfix mt20 mb20">
          <button
            className="btn btn-primary btn-block"
            onClick={() => {
              this.confirmCollectUserDetails();
            }}
            disabled={hasAnyErrors}
          >
            Continue
          </button>
        </div>
      </div>
    );

  }
}

export default props => (
  <AppConsumer>{context => <ClickCollectUser {...props} context={context} />}</AppConsumer>
);
