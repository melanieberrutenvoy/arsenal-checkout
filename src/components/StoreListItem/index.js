import React, { Component } from 'react';

class StoreListItem extends Component {
  render() {
    let store = this.props.store;
    return (
      <div className="col-xs-12 click-collect-address">
        <h3 className="click-collect-address__name">
          {store.displayName}
        </h3>
        <span className="click-collect-address__distance">
          {store.distanceKm} km
        </span>
        <span className="click-collect-address__price">
          {store.deliveryCost.formattedValue}
        </span>
        <div className="click-collect-address__address">
          {store.address.line1}

          <br/>
          {store.address.town} <br/> {store.address.postalCode} <br/>
          {store.address.country.name}
        </div>
        <div className="click-collect-address__actions click-collect-actions">
          <a onClick={()=>this.props.viewStoreDetails(store.name)} data-toggle="modal" data-storeid={store.name} className="click-collect-actions__item click-collect-actions__item--openingtimes">
            <img alt="Opening times" src="/_ui/responsive/common/images/delivery_logos/clock.png" className="click-collect-address__icon click-collect-address__icon--openingtimes" />
            Opening times:
          </a>
          <a onClick={()=>this.props.showOnMap(store.geoPoint.latitude, store.geoPoint.longitude, 15, store.name)}className="click-collect-actions__item click-collect-actions__item--showonmap" data-toggle="tab" data-storeid={store.name} aria-controls="mapview" data-map-zoom="15">
            <img alt="Show on Map" src="/_ui/responsive/common/images/delivery_logos/deliveryMap.png" className="click-collect-address__icon click-collect-address__icon--showonmap" />
            Show on Map
          </a>
          <br />
          <br />
          <button type="button" className="click-collect-address__button" onClick={()=>this.props.selectStore(store.name)} data-storeid={store.name} name="selectLocation-GB13493">Select</button>
        </div>
      </div>
    );
  }
}

export default StoreListItem;
