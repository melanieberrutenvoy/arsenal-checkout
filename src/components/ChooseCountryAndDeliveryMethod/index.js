import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import { countries } from '../../helpers';

class ChooseCountryAndDeliveryMethod extends Component {
	constructor() {
		super();

		this.state = {
			deliveryCountry: '',
			deliveryOption: '',
			deliveryCountryType: ''
		};

		this.handleCountrySelection = this.handleCountrySelection.bind(this);
		this.handleDeliveryMethodSelect = this.handleDeliveryMethodSelect.bind(this);
	}

	componentWillMount() {
		let context = this.props.context.state;

		// Country Selected
		let countrySelected = context.deliveryCountry;
		this.setState({ deliveryCountry: countrySelected });
	}

	handleCountrySelection(e) {
		// Store selected country
		let countrySelected = e.target.value;
		let deliveryCountryType = e.target[e.target.selectedIndex].attributes.getNamedItem('data-delivery-type').value;
		this.props.context.updateDeliveryCountry(countrySelected, deliveryCountryType, true);
		this.setState({ deliveryCountry: countrySelected, deliveryCountryType });
		localStorage.setItem('countrySelected', JSON.stringify(countrySelected));
		localStorage.setItem('deliveryCountryType', JSON.stringify(deliveryCountryType));

		// Fetch the delivery fetchParentDeliveryCategories
		this.props.fetchParentDeliveryCategories(countrySelected, deliveryCountryType);

		// Worldwide deliveries
		// this.props.context.updateDeliveryWorldwideType(deliveryWorldwideType, true);
		// localStorage.setItem('deliveryWorldwideType', JSON.stringify(deliveryWorldwideType));
	}

	handleDeliveryMethodSelect(e, key) {
		this.setState({ deliveryOption: e.target.id });
		this.props.context.updatedeliveryMethodParentSelected(e.target.id, true);
		localStorage.setItem('deliveryMethodSelect', JSON.stringify(e.target.id));

		this.props.createChildDeliveryOptionFromParent(e.target.id);
		this.props.history.push('/delivery#2');
	}

	render() {
		let context = this.props.context.state;
		return (
			<React.Fragment>
				<h1 className="checkout-heading">Choose a delivery method</h1>
				<div className="delivery col-xs-12">
					<div className="form-group form-group--side">
						<label htmlFor="deliverTo" className="form-group__label">
							Delivery to
						</label>

						<select
							className="form-control form-group__input"
							name="nameDeliverTo"
							id="deliverTo"
							value={this.state.deliveryCountry}
							onChange={this.handleCountrySelection}
						>
							<option value="">Select country</option>
							{countries.map((country, i) => (
								<option
									key={i}
									value={country.value}
									data-delivery-type={country.deliveryMethod}
									disabled={country.disabled}
								>
									{country.name}
								</option>
							))}
						</select>
					</div>

					{context.deliveryMethodParent &&
						context.deliveryMethodParent.map((type, i) => (
							<div key={i} className="form-group form-group--radio-selection form-group--list">
								<div className="radio-selection">
									<label
										htmlFor={type.id}
										className={
											context.deliveryMethodParentSelected === type.id
												? 'radio-selection__label radio-selection__label--active'
												: 'radio-selection__label'
										}
									>
										{type.name}
									</label>
									<input
										className="radio-selection__radio"
										type="radio"
										name={'group' + i + type.name}
										id={type.id}
										value={type.name}
										checked={context.deliveryMethodParentSelected === type.id}
										onChange={this.handleDeliveryMethodSelect}
									/>
									<ul className="radio-selection__list list-unstyled">
										{type.options &&
											type.options.map((option, i) => (
												<li key={i} className="radio-selection__item">
													{option.label}, {option.duration},{' '}
													<span className="pull-right">
														<strong>{option.price}</strong>
													</span>
												</li>
											))}
										<li className="radio-selection__item">
											{type.duration}
											<span className="pull-right">
												<strong>{type.price}</strong>
											</span>
										</li>
									</ul>
								</div>
							</div>
						))}
				</div>
			</React.Fragment>
		);
	}
}

export default props => (
	<AppConsumer>{context => <ChooseCountryAndDeliveryMethod {...props} context={context} />}</AppConsumer>
);
