import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import FormLabelElement from '../FormLabelElement';
import { validateNumber, validateAlphanumeric, validatePostcode, validateNotEmpty } from '../../validate';
import { titles } from '../../helpers';

class WorldwideDeliveryForm extends Component {
	constructor() {
		super();

		this.state = {
			titles: titles,
			title: '',
			firstName: '',
			lastName: '',

			houseNumber: '',
			addressLine1: '',
			addressLine2: '',
			addressLine3: '',
			city: '',
			region: '',
			postcode: '',

			errors: {
				title: true,
				firstName: true,
				lastName: true,
				houseNumber: true,
				addressLine1: true,
				addressLine2: false,
				addressLine3: false,
				city: true,
				region: true,
				postcode: true
			}
		};

		this.handleChange = this.handleChange.bind(this);
		this.confirmAddress = this.confirmAddress.bind(this);
	}

	confirmAddress() {
		let fullAddress = [];
		fullAddress.push({
			title:this.state.title, 
			firstName:this.state.firstName, 
			lastName:this.state.lastName,
			houseNumber: this.state.houseNumber,
			addressLine1: this.state.addressLine1,
			addressLine2: this.state.addressLine2,
			addressLine3: this.state.addressLine3,
			city: this.state.city,
			region: this.state.region,
			postcode: this.state.postcode
		});
		this.props.context.updateDeliveryAddress(fullAddress, true);
		localStorage.setItem('addressSelected', JSON.stringify(fullAddress));
	}

	handleChange(evt) {
		this.setState({ [evt.target.name]: evt.target.value });
		let value = evt.target.value;
		let validation = evt.target.attributes.getNamedItem('data-validation').value;
		let result;
		if (validation === 'number') {
			result = validateNumber(value);
		} else if (validation === 'alphanumeric') {
			result = validateAlphanumeric(value);
		} else if (validation === 'postcode') {
			value = value.toUpperCase();
			result = validatePostcode(value);
		} else if (validation === 'notEmpty') {
			result = validateNotEmpty(value);
		}
		this.setState({
			errors: { ...this.state.errors, [evt.target.name]: !result }
		});
	}

	render() {
		return (
			<div className="worldwide-delivery col-xs-12">
				<div className="row">
					<h1 className="checkout-heading">Delivery to</h1>
					<div className="clearfix">
						<FormLabelElement
							handleChange={() => this.handleChange}
							selectOptions={this.state.titles}
							validationType="notEmpty"
							label="Title"
							id="title"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Firstname"
							id="firstName"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Lastname"
							id="lastName"
						/>

						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="House number"
							id="houseNumber"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Address Line"
							id="addressLine1"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Address Line 2"
							id="addressLine2"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Address Line 3"
							id="addressLine3"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="City"
							id="city"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="alphanumeric"
							label="Region"
							id="region"
						/>
						<FormLabelElement
							handleChange={() => this.handleChange}
							validationType="notEmpty"
							label="Zip / Postal code"
							id="postcode"
						/>
						<button className="btn btn-primary btn-block" onClick={this.confirmAddress}>
							Use this address
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default props => <AppConsumer>{context => <WorldwideDeliveryForm {...props} context={context} />}</AppConsumer>;
