import React, { Component } from 'react';

class CheckoutSteps extends Component {
  render() {
    return (
      <div className="container">
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-4 col-md-push-4 no_pad">
          <div className="steps col-xs-12">
            <div
              className={
                'steps__step col-xs-4' + (this.props.activeStep === 1 ? ' steps__step--active' : '')
              }
            >
              <div className="steps__count">
                <div className="steps__number">1</div>
              </div>
              <div className="steps__name">Delivery</div>
            </div>

            <div
              className={
                'steps__step col-xs-4' + (this.props.activeStep === 2 ? ' steps__step--active' : '')
              }
            >
              <div className="steps__count">
                <div className="steps__number">2</div>
              </div>
              <div className="steps__name">Pay</div>
            </div>

            <div
              className={
                'steps__step col-xs-4' + (this.props.activeStep === 3 ? ' steps__step--active' : '')
              }
            >
              <div className="steps__count">
                <div className="steps__number">3</div>
              </div>
              <div className="steps__name">Confirm</div>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default CheckoutSteps;
