import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import endpoints from '../../endpoints';
import axios from 'axios';

class NominatedDay extends Component {
	constructor() {
		super();

		this.state = {
			days: [],
			times: [],
			daySelected: '',
			timeSelected: '',
			costSelected: ''
		};

		this.daySelect = this.daySelect.bind(this);
	}

	componentWillMount() {
		axios.get(endpoints.deliveryOptions).then(response => {
			this.setState({ days: response.data });
		});

		let deliveryNominated = JSON.parse(localStorage.getItem('deliveryNominated')) || '';
		this.setState({ daySelected: deliveryNominated.day, timeSelected: deliveryNominated.time, costSelected: deliveryNominated.cost });

		deliveryNominated ? this.fetchTimes() :  this.setState({ times: [] });
	}

	fetchTimes() {
		axios.get(endpoints.deliveryCost).then(response => {
			this.setState({ times: response.data });
		});
	}

	daySelect(e, dayOfWeek, day) {
		this.setState({ daySelected: day });
		this.fetchTimes();
	}

	costSelect(time, cost) {
		this.setState({ timeSelected: time, costSelected: cost });
		let deliveryNominated = { day: this.state.daySelected, time: time, cost: cost };
		// If nominated, store day / time
		this.props.context.updateNominatedSelection(this.state.daySelected, time, cost, true);
		localStorage.setItem('deliveryNominated', JSON.stringify(deliveryNominated));
	}

	formatTime(time) {
		if (time === 'AM') {
			return 'Before midday';
		} else if (time === 'PM') {
			return 'After midday';
		} else if (time === 'ALL_DAY') {
			return 'All day';
		} else {
			return time;
		}
	}

	render() {
		const day = this.state.days.map((day, i) => (
			<div
				key={i}
				className="col-xs-4 col-md-3 col-lg-2 nominated_delivery__column"
				onClick={e => this.daySelect(e, day.dayOfWeek, day.day)}
			>
				<div
					className={
						this.state.daySelected === day.day
							? 'nominated_delivery__block nominated_delivery__block--active'
							: 'nominated_delivery__block'
					}
					data-reference="1"
				>
					<p className="nominated_delivery__date">{day.day}</p>
				</div>
			</div>
		));

		const time =
			this.state.times.map((time, i) => (
				<div
					key={i}
					className="col-xs-12 col-md-6 nominated_delivery__slot"
					onClick={() => this.costSelect(time.timeSlot, time.deliveryCost.value)}
				>
					<div
						className={
							this.state.timeSelected === time.timeSlot
								? 'nominated_delivery__slot-content js-select-slot active'
								: 'nominated_delivery__slot-content js-select-slot'
						}
					>
						{this.formatTime(time.timeSlot)}
						&nbsp;
						<span className="deliverycost">{time.deliveryCost.formattedValue}</span>
					</div>
				</div>
			));

		return (
			<div className="nominated-day">
				<h1 className="checkout-heading">Choose a date</h1>
				<div className="nominated_delivery-container">
					<div className="nominated_delivery__wrapper">{day}</div>
					<div className="nominated_delivery__day-slots">{time}</div>
				</div>
			</div>
		);
	}
}

export default props => <AppConsumer>{context => <NominatedDay {...props} context={context} />}</AppConsumer>;
