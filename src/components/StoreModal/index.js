import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import { isChild } from '../../utils.js';


class StoreModal extends Component {

	constructor() {
		super();

		this.checkIfModalShouldClose = this.checkIfModalShouldClose.bind(this);
	}

	checkIfModalShouldClose(e) {
		if(e.target === this.closeModal) {
			this.props.context.updateModalVisibility(false, true);
		} else {
			let dismissModal = isChild(this.modalContent,e.target);
			this.props.context.updateModalVisibility(!dismissModal, true);
		}
	}

	render() {
		let store = this.props.store;
		if(store) {
			return (
				<div
					ref="modal"
					aria-hidden={ this.props.isActive ? 'false' : 'true'}
					aria-labelledby="clickCollectOpeningTimesLabel"
					className={ this.props.isActive ? 'modal fade in' : 'modal fade'}
					id="clickCollectOpeningTimes"
					role="dialog"
					tabIndex="-1"
					style={{display: this.props.isActive ? 'block' : 'none'}}
					onClick={(e) => this.checkIfModalShouldClose(e)}
					
				>
					<div className="modal-dialog opening-times-modal" role="document">
						<div ref={(ref) => this.modalContent = ref} className="modal-content opening-times-modal__content">
							<button
								ref={(ref) => this.closeModal = ref}
								aria-label="Close"
								className="close opening-times-modal__close"
								data-dismiss="modal"
								type="button"
							>
								×
							</button>
							<div className="modal-body opening-times-modal__body click-collect-address">
								<h3 className="click-collect-address__name">{store.displayName}</h3>
							
								<span className="click-collect-address__distance">{store.distanceKm} km</span>
								<span className="click-collect-address__price">{store.deliveryCost.formattedValue}</span>
								<div className="click-collect-address__address">
									{store.address.line1}

									<br/>
									{store.address.town} <br/> {store.address.postalCode} <br/>
									{store.address.country.name}
								</div>
								<p className="click-collect-address__heading">
									<img
										alt="opening times"
										className="click-collect-address__icon click-collect-address__icon--openingtimes"
										src="/_ui/responsive/common/images/delivery_logos/clock.png"
									/>{' '}
									Opening times:
								</p>
								<table className="click-collect-address__openingtimes opening-times-table">
									<tbody>
										{store.openingHours.weekDayOpeningList.map((opening, i) => (
											<tr key={i} className="opening-times-table__row">
												<td className="opening-times-table__cell opening-times-table__cell--day">
													<span>{opening.weekDay}</span>
												</td>
												<td className="opening-times-table__cell opening-times-table__cell--times">
													<span>{opening.openingTime.formattedHour}</span> - <span>{opening.closingTime.formattedHour}</span>
												</td>
											</tr>
										))}
									</tbody>
								</table>
								<div className="click-collect-address__actions click-collect-actions">
									<button
										onClick={()=>this.props.selectStore(store.name)}
										className="click-collect-address__button"
										type="button"
									>
										Select
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (<div></div>);
		}
	}
}

export default props => <AppConsumer>{context => <StoreModal {...props} context={context} />}</AppConsumer>;
