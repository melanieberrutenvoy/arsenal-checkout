import React, { Component } from 'react';
import FormLabelElement from '../FormLabelElement';
import { AppConsumer } from '../../AppContext';
import endpoints from '../../endpoints';
import { titles } from '../../helpers';
import { validateNumber, validateAlphanumeric, validateEmail, validatePostcode, validateNotEmpty } from '../../validate';
import axios from 'axios';


class AddressFormValidation extends Component {
  constructor() {
    super();

    this.state = {
      title: '',
      firstName: '',
      lastName: '',
      mobile: '',
      email: '',
      houseNumber: '',
      addressLine1: '',
      addressLine2: '',
      postcode: '',
      addresses: '',
      addressSelected: '',
      titles: titles,
      validate: false,
      isManual: false,
      errors: {
        title: true,
        firstName: true,
        lastName: true,
        mobile: true,
        email: true,
        houseNumber: true,
        postcode: true
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.validateAddress = this.validateAddress.bind(this);
    this.confirmAddress = this.confirmAddress.bind(this);
    this.selectingAddress = this.selectingAddress.bind(this);
    this.enterManualAddress = this.enterManualAddress.bind(this);
  }

  componentDidMount() {
    let addressSelected = JSON.parse(localStorage.getItem('addressSelected')) || '';
    this.props.context.updateDeliveryAddress(addressSelected, true);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
    let value = evt.target.value;
    let validation = evt.target.attributes.getNamedItem('data-validation').value;
    let result;

    if(validation === 'number') {
      result = validateNumber(value);
    } else if(validation === 'alphanumeric') {
      result = validateAlphanumeric(value);
    } else if(validation === 'email') {
      result = validateEmail(value);
    } else if(validation === 'postcode') {
      value = value.toUpperCase();
      result = validatePostcode(value);
    } else if(validation === 'notEmpty') {
      result = validateNotEmpty(value);
    }

    this.setState({
      errors: { ...this.state.errors, [evt.target.name]: !result },
    });
  }

  validateAddress() {
    console.info('You are asking postcode lookup for ', this.state.postcode);
    this.setState(prevState => ({ validate: true }));
    axios.get(endpoints.address).then(response => {
      let res = response.data;
      let result = Object.keys(res).map(k => res[k]);
      this.setState(prevState => ({ addresses: result }));
    });
  }

  selectingAddress(e, key) {
    let addressSelected = e.target.value;
    this.setState(prevState => ({ addressSelected }));
  }

  confirmAddress() {
    let fullAddress = [];
    fullAddress.push(
      this.state.title,
      this.state.firstName,
      this.state.lastName,
      this.state.addressSelected
    );
    this.props.context.updateDeliveryAddress(fullAddress, true);
    localStorage.setItem('addressSelected', JSON.stringify(fullAddress));
  }

  confirmManualAddress() {
    let fullAddress = [];
    let addressSelected =
      this.state.houseNumber +
      ' ' +
      this.state.addressLine1 +
      ', ' +
      this.state.addressLine2 +
      ', ' +
      this.state.postcode;

    fullAddress.push(this.state.title, this.state.firstName, this.state.lastName, addressSelected);
    this.props.context.updateDeliveryAddress(fullAddress, true);
    localStorage.setItem('addressSelected', JSON.stringify(fullAddress));
  }

  enterManualAddress() {
    this.setState(prevState => ({ isManual: true }));
  }

  render() {
    let hasAnyErrors =
      this.state.errors.title ||
      this.state.errors.firstName ||
      this.state.errors.lastName ||
      this.state.errors.mobile ||
      this.state.errors.email ||
      this.state.errors.houseNumber ||
      this.state.errors.postcode;

    if (this.state.isManual) {
      return (
        <div className="address-block address-block--manual">
          <h1 className="checkout-heading">{this.props.firstStepTitle}</h1>
          <div className="clearfix">
            <FormLabelElement
              handleChange={() => this.handleChange}
              selectOptions={this.state.titles}
              validationType="notEmpty"
              label="Title"
              id="title"
            />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="alphanumeric"
              label="Firstname"
              id="firstName"
            />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="alphanumeric"
              label="Lastname"
              id="lastName"
            />
            <FormLabelElement 
              handleChange={() => this.handleChange} 
              validationType="number"
              label="Mobile" 
              id="mobile" />
            <FormLabelElement 
              handleChange={() => this.handleChange} 
              validationType="email"
              label="Email" 
              id="email" />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="alphanumeric"
              label="House number"
              id="houseNumber"
            />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="alphanumeric"
              label="Address Line"
              id="addressLine1"
            />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="alphanumeric"
              label="Address Line 2"
              id="addressLine2"
            />
            <FormLabelElement
              handleChange={() => this.handleChange}
              validationType="postcode"
              label="Postcode"
              id="postcode"
            />
          </div>
          <div className="clearfix mt20 mb20">
            <button
              className="btn btn-primary btn-block"
              onClick={() => {
                this.confirmManualAddress();
                this.props.confirmTheAddress();
              }}
              disabled={hasAnyErrors}
            >
              Use this address
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="address-block">
          {this.props.context.state.deliveryAddress.length === 0 &&
            this.state.validate === false && (
              <React.Fragment>
                <h1 className="checkout-heading">{this.props.firstStepTitle}</h1>
                <div className="clearfix">
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="notEmpty"
                    selectOptions={this.state.titles}
                    label="Title"
                    id="title"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="alphanumeric"
                    label="Firstname"
                    id="firstName"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="alphanumeric"
                    label="Lastname"
                    id="lastName"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="number"
                    label="Mobile"
                    id="mobile"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="email"
                    label="Email"
                    id="email"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="alphanumeric"
                    label="House number"
                    id="houseNumber"
                  />
                  <FormLabelElement
                    handleChange={() => this.handleChange}
                    validationType="postcode"
                    label="Postcode"
                    id="postcode"
                  />
                </div>
                <div className="clearfix mt20 mb20">
                  <button
                    className="btn btn-primary btn-block"
                    onClick={() => {
                      this.validateAddress();
                    }}
                    disabled={hasAnyErrors}
                  >
                    Validate address
                  </button>
                </div>
              </React.Fragment>
            )}

          {this.state.addresses.length > 0 &&
            this.state.validate === true && (
              <div className="valide-addresses mt20">
                <h1 className="checkout-heading">{this.props.secondStepTitle}</h1>
                <div className="form-group">
                  <label htmlFor="address-list" className="control-label sr-only">
                    Select an address
                  </label>
                  <select
                    onChange={this.selectingAddress}
                    name="address-list"
                    className="form-control mb20"
                    value={this.state.addressSelected}
                    id="address-list"
                  >
                    <option value="">Please select</option>
                    {this.state.addresses.map((address, i) => (
                      <option key={i} value={address}>
                        {address}
                      </option>
                    ))}
                  </select>
                  <p>
                    <a className="btn-link btn-link--secondary btn-link--underline" onClick={this.enterManualAddress}>
                      Or enter address manually
                    </a>
                  </p>
                  <div className="clearfix mt20 mb20">
                    <button
                      className="btn btn-primary btn-block"
                      onClick={() => {
                        this.confirmAddress();
                        this.props.confirmTheAddress();
                      }}
                    >
                      Use this address
                    </button>
                  </div>
                </div>
              </div>
            )}
        </div>
      );
    }
  }
}

export default props => (
  <AppConsumer>{context => <AddressFormValidation {...props} context={context} />}</AppConsumer>
);
