const baseUrl = 'http://localhost:300';
const endpoints = {
  address: `${baseUrl}1/address`,
  delivery: `${baseUrl}2/delivery`,
  deliveryCost: `${baseUrl}3/deliveryCost`,
  deliveryOptions: `${baseUrl}4/deliveryOptions`,
  payment: `${baseUrl}5/payment`,
  userDetails: `${baseUrl}6/userDetails`,
  collectionPointOfServices: `${baseUrl}7/collectionPointOfServices`,
  worldwideDelivery: `${baseUrl}8/worldwideDelivery`
};

export default endpoints;
