export function objToMap(obj) {
  return Object.keys(obj).map(k => obj[k]);
}

export function isChild (obj,parentObj){
    while (obj !== undefined && obj !== null && obj.tagName.toUpperCase() !== 'BODY'){
        if (obj === parentObj){
            return true;
        }
        obj = obj.parentNode;
    }
    return false;
}