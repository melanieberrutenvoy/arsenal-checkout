
export function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function validateNumber(value) {
	return !isNaN(parseFloat(value)) && isFinite(value);
}

export function validateAlphanumeric(value) {
	let regex = /^[a-z0-9]+$/i;
	return regex.test(value);
}

export function validatePostcode(value) {
	let regex = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
	return regex.test(value);
}

export function validateNotEmpty(value) {
	return value.length > 0;
}