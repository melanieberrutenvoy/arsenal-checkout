export const countries = [
	{
	'value': 'UK',
	'deliveryMethod': 'UK',
	'name': 'United Kingdom'
	},{
	'value': 'IE',
	'deliveryMethod': 'CAT1',
	'name': 'Ireland'
	},{
	'value': 'US',
	'deliveryMethod': 'CAT1',
	'name': 'United States'
	},{
	'value': 'NO',
	'deliveryMethod': 'CAT2',
	'name': 'Norway'
	},{
	'value': 'SE',
	'deliveryMethod': 'CAT2',
	'name': 'Sweden'
	},{
	'value': '',
	'deliveryMethod': '',
	'name': '',
	'disabled': true
	},{
	'value': 'AF',
	'deliveryMethod': 'INT',
	'name': 'Afghanistan'
	},{
	'value': 'AL',
	'deliveryMethod': 'INT',
	'name': 'Albania'
	},{
	'value': 'DZ',
	'deliveryMethod': 'INT',
	'name': 'Algeria'
	},{
	'value': 'AS',
	'deliveryMethod': 'INT',
	'name': 'American Samoa'
	},{
	'value': 'AD',
	'deliveryMethod': 'INT',
	'name': 'Andorra'
	},{
	'value': 'AO',
	'deliveryMethod': 'INT',
	'name': 'Angola'
	},{
	'value': 'AI',
	'deliveryMethod': 'INT',
	'name': 'Anguilla'
	},{
	'value': 'AQ',
	'deliveryMethod': 'INT',
	'name': 'Antarctica'
	},{
	'value': 'AG',
	'deliveryMethod': 'INT',
	'name': 'Antigua and Barbuda'
	},{
	'value': 'AR',
	'deliveryMethod': 'INT',
	'name': 'Argentina'
	},{
	'value': 'AM',
	'deliveryMethod': 'INT',
	'name': 'Armenia'
	},{
	'value': 'AW',
	'deliveryMethod': 'INT',
	'name': 'Aruba'
	},{
	'value': 'AU',
	'deliveryMethod': 'CAT1',
	'name': 'Australia'
	},{
	'value': 'AT',
	'deliveryMethod': 'EU',
	'name': 'Austria'
	},{
	'value': 'AZ',
	'deliveryMethod': 'INT',
	'name': 'Azerbaijan'
	},{
	'value': 'BS',
	'deliveryMethod': 'INT',
	'name': 'Bahamas'
	},{
	'value': 'BH',
	'deliveryMethod': 'INT',
	'name': 'Bahrain'
	},{
	'value': 'BD',
	'deliveryMethod': 'INT',
	'name': 'Bangladesh'
	},{
	'value': 'BB',
	'deliveryMethod': 'INT',
	'name': 'Barbados'
	},{
	'value': 'BY',
	'deliveryMethod': 'INT',
	'name': 'Belarus'
	},{
	'value': 'BE',
	'deliveryMethod': 'EU',
	'name': 'Belgium'
	},{
	'value': 'BZ',
	'deliveryMethod': 'INT',
	'name': 'Belize'
	},{
	'value': 'BJ',
	'deliveryMethod': 'INT',
	'name': 'Benin'
	},{
	'value': 'BM',
	'deliveryMethod': 'INT',
	'name': 'Bermuda'
	},{
	'value': 'BT',
	'deliveryMethod': 'INT',
	'name': 'Bhutan'
	},{
	'value': 'BO',
	'deliveryMethod': 'INT',
	'name': 'Bolivia'
	},{
	'value': 'BA',
	'deliveryMethod': 'INT',
	'name': 'Bosnia and Herzegovina'
	},{
	'value': 'BW',
	'deliveryMethod': 'INT',
	'name': 'Botswana'
	},{
	'value': 'BV',
	'deliveryMethod': 'INT',
	'name': 'Bouvet Island'
	},{
	'value': 'BR',
	'deliveryMethod': 'INT',
	'name': 'Brazil'
	},{
	'value': 'IO',
	'deliveryMethod': 'INT',
	'name': 'British Indian Ocean Territory'
	},{
	'value': 'BN',
	'deliveryMethod': 'INT',
	'name': 'Brunei Darussalam'
	},{
	'value': 'BG',
	'deliveryMethod': 'EU',
	'name': 'Bulgaria'
	},{
	'value': 'BF',
	'deliveryMethod': 'INT',
	'name': 'Burkina Faso'
	},{
	'value': 'BI',
	'deliveryMethod': 'INT',
	'name': 'Burundi'
	},{
	'value': 'KH',
	'deliveryMethod': 'INT',
	'name': 'Cambodia'
	},{
	'value': 'CM',
	'deliveryMethod': 'INT',
	'name': 'Cameroon'
	},{
	'value': 'CA',
	'deliveryMethod': 'INT',
	'name': 'Canada'
	},{
	'value': 'CV',
	'deliveryMethod': 'INT',
	'name': 'Cape Verde'
	},{
	'value': 'BQ',
	'deliveryMethod': 'INT',
	'name': 'Caribbean Netherlands'
	},{
	'value': 'KY',
	'deliveryMethod': 'INT',
	'name': 'Cayman Islands'
	},{
	'value': 'CF',
	'deliveryMethod': 'INT',
	'name': 'Central African Republic'
	},{
	'value': 'TD',
	'deliveryMethod': 'INT',
	'name': 'Chad'
	},{
	'value': 'CL',
	'deliveryMethod': 'INT',
	'name': 'Chile'
	},{
	'value': 'CN',
	'deliveryMethod': 'INT',
	'name': 'China'
	},{
	'value': 'CX',
	'deliveryMethod': 'INT',
	'name': 'Christmas Island'
	},{
	'value': 'CC',
	'deliveryMethod': 'INT',
	'name': 'Cocos (Keeling) Islands'
	},{
	'value': 'CO',
	'deliveryMethod': 'INT',
	'name': 'Colombia'
	},{
	'value': 'KM',
	'deliveryMethod': 'INT',
	'name': 'Comoros'
	},{
	'value': 'CG',
	'deliveryMethod': 'INT',
	'name': 'Congo'
	},{
	'value': 'CD',
	'deliveryMethod': 'INT',
	'name': 'Congo, Democratic Republic of'
	},{
	'value': 'CK',
	'deliveryMethod': 'INT',
	'name': 'Cook Islands'
	},{
	'value': 'CR',
	'deliveryMethod': 'INT',
	'name': 'Costa Rica'
	},{
	'value': 'HR',
	'deliveryMethod': 'EU',
	'name': 'Croatia'
	},{
	'value': 'CU',
	'deliveryMethod': 'INT',
	'name': 'Cuba'
	},{
	'value': 'CW',
	'deliveryMethod': 'INT',
	'name': 'Curaçao'
	},{
	'value': 'CY',
	'deliveryMethod': 'EU',
	'name': 'Cyprus'
	},{
	'value': 'CZ',
	'deliveryMethod': 'EU',
	'name': 'Czech Republic'
	},{
	'value': 'CI',
	'deliveryMethod': 'INT',
	'name': "Côte d'Ivoire"
	},{
	'value': 'DK',
	'deliveryMethod': 'EU',
	'name': 'Denmark'
	},{
	'value': 'DJ',
	'deliveryMethod': 'INT',
	'name': 'Djibouti'
	},{
	'value': 'DM',
	'deliveryMethod': 'INT',
	'name': 'Dominica'
	},{
	'value': 'DO',
	'deliveryMethod': 'INT',
	'name': 'Dominican Republic'
	},{
	'value': 'EC',
	'deliveryMethod': 'INT',
	'name': 'Ecuador'
	},{
	'value': 'EG',
	'deliveryMethod': 'INT',
	'name': 'Egypt'
	},{
	'value': 'SV',
	'deliveryMethod': 'INT',
	'name': 'El Salvador'
	},{
	'value': 'GQ',
	'deliveryMethod': 'INT',
	'name': 'Equatorial Guinea'
	},{
	'value': 'ER',
	'deliveryMethod': 'INT',
	'name': 'Eritrea'
	},{
	'value': 'EE',
	'deliveryMethod': 'EU',
	'name': 'Estonia'
	},{
	'value': 'ET',
	'deliveryMethod': 'INT',
	'name': 'Ethiopia'
	},{
	'value': 'FK',
	'deliveryMethod': 'INT',
	'name': 'Falkland Islands'
	},{
	'value': 'FO',
	'deliveryMethod': 'INT',
	'name': 'Faroe Islands'
	},{
	'value': 'FJ',
	'deliveryMethod': 'INT',
	'name': 'Fiji'
	},{
	'value': 'FI',
	'deliveryMethod': 'EU',
	'name': 'Finland'
	},{
	'value': 'FR',
	'deliveryMethod': 'EU',
	'name': 'France'
	},{
	'value': 'FX',
	'deliveryMethod': 'EU',
	'name': 'France, Metropolitan'
	},{
	'value': 'GF',
	'deliveryMethod': 'INT',
	'name': 'French Guiana'
	},{
	'value': 'PF',
	'deliveryMethod': 'INT',
	'name': 'French Polynesia'
	},{
	'value': 'TF',
	'deliveryMethod': 'INT',
	'name': 'French Southern Territories'
	},{
	'value': 'GA',
	'deliveryMethod': 'INT',
	'name': 'Gabon'
	},{
	'value': 'GM',
	'deliveryMethod': 'INT',
	'name': 'Gambia'
	},{
	'value': 'GE',
	'deliveryMethod': 'INT',
	'name': 'Georgia'
	},{
	'value': 'DE',
	'deliveryMethod': 'EU',
	'name': 'Germany'
	},{
	'value': 'GH',
	'deliveryMethod': 'INT',
	'name': 'Ghana'
	},{
	'value': 'GI',
	'deliveryMethod': 'INT',
	'name': 'Gibraltar'
	},{
	'value': 'GR',
	'deliveryMethod': 'EU',
	'name': 'Greece'
	},{
	'value': 'GL',
	'deliveryMethod': 'INT',
	'name': 'Greenland'
	},{
	'value': 'GD',
	'deliveryMethod': 'INT',
	'name': 'Grenada'
	},{
	'value': 'GP',
	'deliveryMethod': 'INT',
	'name': 'Guadeloupe'
	},{
	'value': 'GU',
	'deliveryMethod': 'INT',
	'name': 'Guam'
	},{
	'value': 'GT',
	'deliveryMethod': 'INT',
	'name': 'Guatemala'
	},{
	'value': 'GG',
	'deliveryMethod': 'INT',
	'name': 'Guernsey'
	},{
	'value': 'GN',
	'deliveryMethod': 'INT',
	'name': 'Guinea'
	},{
	'value': 'GW',
	'deliveryMethod': 'INT',
	'name': 'Guinea-Bissau'
	},{
	'value': 'GY',
	'deliveryMethod': 'INT',
	'name': 'Guyana'
	},{
	'value': 'HT',
	'deliveryMethod': 'INT',
	'name': 'Haiti'
	},{
	'value': 'HM',
	'deliveryMethod': 'INT',
	'name': 'Heard and McDonald Islands'
	},{
	'value': 'HN',
	'deliveryMethod': 'INT',
	'name': 'Honduras'
	},{
	'value': 'HK',
	'deliveryMethod': 'INT',
	'name': 'Hong Kong'
	},{
	'value': 'HU',
	'deliveryMethod': 'EU',
	'name': 'Hungary'
	},{
	'value': 'IS',
	'deliveryMethod': 'INT',
	'name': 'Iceland'
	},{
	'value': 'IN',
	'deliveryMethod': 'INT',
	'name': 'India'
	},{
	'value': 'ID',
	'deliveryMethod': 'INT',
	'name': 'Indonesia'
	},{
	'value': 'IR',
	'deliveryMethod': 'INT',
	'name': 'Iran'
	},{
	'value': 'IE',
	'deliveryMethod': 'CAT1',
	'name': 'Ireland'
	},{
	'value': 'IQ',
	'deliveryMethod': 'INT',
	'name': 'Iraq'
	},{
	'value': 'IM',
	'deliveryMethod': 'INT',
	'name': 'Isle of Man'
	},{
	'value': 'IL',
	'deliveryMethod': 'INT',
	'name': 'Israel'
	},{
	'value': 'IT',
	'deliveryMethod': 'INT',
	'name': 'Italy'
	},{
	'value': 'JM',
	'deliveryMethod': 'INT',
	'name': 'Jamaica'
	},{
	'value': 'JP',
	'deliveryMethod': 'INT',
	'name': 'Japan'
	},{
	'value': 'JE',
	'deliveryMethod': 'INT',
	'name': 'Jersey'
	},{
	'value': 'JO',
	'deliveryMethod': 'INT',
	'name': 'Jordan'
	},{
	'value': 'KZ',
	'deliveryMethod': 'INT',
	'name': 'Kazakhstan'
	},{
	'value': 'KE',
	'deliveryMethod': 'INT',
	'name': 'Kenya'
	},{
	'value': 'KI',
	'deliveryMethod': 'INT',
	'name': 'Kiribati'
	},{
	'value': 'XK',
	'deliveryMethod': 'INT',
	'name': 'Kosovo'
	},{
	'value': 'KW',
	'deliveryMethod': 'INT',
	'name': 'Kuwait'
	},{
	'value': 'KG',
	'deliveryMethod': 'INT',
	'name': 'Kyrgyzstan'
	},{
	'value': 'LA',
	'deliveryMethod': 'INT',
	'name': "Lao People's Democratic Republic"
	},{
	'value': 'LV',
	'deliveryMethod': 'EU',
	'name': 'Latvia'
	},{
	'value': 'LB',
	'deliveryMethod': 'INT',
	'name': 'Lebanon'
	},{
	'value': 'LS',
	'deliveryMethod': 'INT',
	'name': 'Lesotho'
	},{
	'value': 'LR',
	'deliveryMethod': 'INT',
	'name': 'Liberia'
	},{
	'value': 'LY',
	'deliveryMethod': 'INT',
	'name': 'Libya'
	},{
	'value': 'LI',
	'deliveryMethod': 'INT',
	'name': 'Liechtenstein'
	},{
	'value': 'LT',
	'deliveryMethod': 'EU',
	'name': 'Lithuania'
	},{
	'value': 'LU',
	'deliveryMethod': 'EU',
	'name': 'Luxembourg'
	},{
	'value': 'MO',
	'deliveryMethod': 'INT',
	'name': 'Macau'
	},{
	'value': 'MK',
	'deliveryMethod': 'INT',
	'name': 'Macedonia'
	},{
	'value': 'MG',
	'deliveryMethod': 'INT',
	'name': 'Madagascar'
	},{
	'value': 'MW',
	'deliveryMethod': 'INT',
	'name': 'Malawi'
	},{
	'value': 'MY',
	'deliveryMethod': 'INT',
	'name': 'Malaysia'
	},{
	'value': 'MV',
	'deliveryMethod': 'INT',
	'name': 'Maldives'
	},{
	'value': 'ML',
	'deliveryMethod': 'INT',
	'name': 'Mali'
	},{
	'value': 'MT',
	'deliveryMethod': 'EU',
	'name': 'Malta'
	},{
	'value': 'MH',
	'deliveryMethod': 'INT',
	'name': 'Marshall Islands'
	},{
	'value': 'MQ',
	'deliveryMethod': 'INT',
	'name': 'Martinique'
	},{
	'value': 'MR',
	'deliveryMethod': 'INT',
	'name': 'Mauritania'
	},{
	'value': 'MU',
	'deliveryMethod': 'INT',
	'name': 'Mauritius'
	},{
	'value': 'YT',
	'deliveryMethod': 'INT',
	'name': 'Mayotte'
	},{
	'value': 'MX',
	'deliveryMethod': 'INT',
	'name': 'Mexico'
	},{
	'value': 'FM',
	'deliveryMethod': 'INT',
	'name': 'Micronesia, Federated States of'
	},{
	'value': 'MD',
	'deliveryMethod': 'INT',
	'name': 'Moldova'
	},{
	'value': 'MC',
	'deliveryMethod': 'INT',
	'name': 'Monaco'
	},{
	'value': 'MN',
	'deliveryMethod': 'INT',
	'name': 'Mongolia'
	},{
	'value': 'ME',
	'deliveryMethod': 'INT',
	'name': 'Montenegro'
	},{
	'value': 'MS',
	'deliveryMethod': 'INT',
	'name': 'Montserrat'
	},{
	'value': 'MA',
	'deliveryMethod': 'INT',
	'name': 'Morocco'
	},{
	'value': 'MZ',
	'deliveryMethod': 'INT',
	'name': 'Mozambique'
	},{
	'value': 'MM',
	'deliveryMethod': 'INT',
	'name': 'Myanmar'
	},{
	'value': 'NA',
	'deliveryMethod': 'INT',
	'name': 'Namibia'
	},{
	'value': 'NR',
	'deliveryMethod': 'INT',
	'name': 'Nauru'
	},{
	'value': 'NP',
	'deliveryMethod': 'INT',
	'name': 'Nepal'
	},{
	'value': 'NC',
	'deliveryMethod': 'INT',
	'name': 'New Caledonia'
	},{
	'value': 'NZ',
	'deliveryMethod': 'INT',
	'name': 'New Zealand'
	},{
	'value': 'NI',
	'deliveryMethod': 'INT',
	'name': 'Nicaragua'
	},{
	'value': 'NE',
	'deliveryMethod': 'INT',
	'name': 'Niger'
	},{
	'value': 'NG',
	'deliveryMethod': 'INT',
	'name': 'Nigeria'
	},{
	'value': 'NU',
	'deliveryMethod': 'INT',
	'name': 'Niue'
	},{
	'value': 'NF',
	'deliveryMethod': 'INT',
	'name': 'Norfolk Island'
	},{
	'value': 'KP',
	'deliveryMethod': 'INT',
	'name': 'North Korea'
	},{
	'value': 'MP',
	'deliveryMethod': 'INT',
	'name': 'Northern Mariana Islands'
	},{
	'value': 'NO',
	'deliveryMethod': 'CAT2',
	'name': 'Norway'
	},{
	'value': 'OM',
	'deliveryMethod': 'INT',
	'name': 'Oman'
	},{
	'value': 'PK',
	'deliveryMethod': 'INT',
	'name': 'Pakistan'
	},{
	'value': 'PW',
	'deliveryMethod': 'INT',
	'name': 'Palau'
	},{
	'value': 'PS',
	'deliveryMethod': 'INT',
	'name': 'Palestine, State of'
	},{
	'value': 'PA',
	'deliveryMethod': 'INT',
	'name': 'Panama'
	},{
	'value': 'PG',
	'deliveryMethod': 'INT',
	'name': 'Papua New Guinea'
	},{
	'value': 'PY',
	'deliveryMethod': 'INT',
	'name': 'Paraguay'
	},{
	'value': 'PE',
	'deliveryMethod': 'INT',
	'name': 'Peru'
	},{
	'value': 'PH',
	'deliveryMethod': 'INT',
	'name': 'Philippines'
	},{
	'value': 'PN',
	'deliveryMethod': 'INT',
	'name': 'Pitcairn'
	},{
	'value': 'PL',
	'deliveryMethod': 'EU',
	'name': 'Poland'
	},{
	'value': 'PT',
	'deliveryMethod': 'EU',
	'name': 'Portugal'
	},{
	'value': 'PR',
	'deliveryMethod': 'INT',
	'name': 'Puerto Rico'
	},{
	'value': 'QA',
	'deliveryMethod': 'INT',
	'name': 'Qatar'
	},{
	'value': 'RO',
	'deliveryMethod': 'EU',
	'name': 'Romania'
	},{
	'value': 'RU',
	'deliveryMethod': 'INT',
	'name': 'Russian Federation'
	},{
	'value': 'RW',
	'deliveryMethod': 'INT',
	'name': 'Rwanda'
	},{
	'value': 'RE',
	'deliveryMethod': 'INT',
	'name': 'Réunion'
	},{
	'value': 'BL',
	'deliveryMethod': 'INT',
	'name': 'Saint Barthélemy'
	},{
	'value': 'SH',
	'deliveryMethod': 'INT',
	'name': 'Saint Helena'
	},{
	'value': 'KN',
	'deliveryMethod': 'INT',
	'name': 'Saint Kitts and Nevis'
	},{
	'value': 'LC',
	'deliveryMethod': 'INT',
	'name': 'Saint Lucia'
	},{
	'value': 'VC',
	'deliveryMethod': 'INT',
	'name': 'Saint Vincent and the Grenadines'
	},{
	'value': 'MF',
	'deliveryMethod': 'INT',
	'name': 'Saint-Martin (France)'
	},{
	'value': 'WS',
	'deliveryMethod': 'INT',
	'name': 'Samoa'
	},{
	'value': 'SM',
	'deliveryMethod': 'INT',
	'name': 'San Marino'
	},{
	'value': 'ST',
	'deliveryMethod': 'INT',
	'name': 'Sao Tome and Principe'
	},{
	'value': 'SA',
	'deliveryMethod': 'INT',
	'name': 'Saudi Arabia'
	},{
	'value': 'SN',
	'deliveryMethod': 'INT',
	'name': 'Senegal'
	},{
	'value': 'RS',
	'deliveryMethod': 'INT',
	'name': 'Serbia'
	},{
	'value': 'SC',
	'deliveryMethod': 'INT',
	'name': 'Seychelles'
	},{
	'value': 'SL',
	'deliveryMethod': 'INT',
	'name': 'Sierra Leone'
	},{
	'value': 'SG',
	'deliveryMethod': 'INT',
	'name': 'Singapore'
	},{
	'value': 'SX',
	'deliveryMethod': 'INT',
	'name': 'Sint Maarten (Dutch part)'
	},{
	'value': 'SK',
	'deliveryMethod': 'EU',
	'name': 'Slovakia'
	},{
	'value': 'SI',
	'deliveryMethod': 'EU',
	'name': 'Slovenia'
	},{
	'value': 'SB',
	'deliveryMethod': 'INT',
	'name': 'Solomon Islands'
	},{
	'value': 'SO',
	'deliveryMethod': 'INT',
	'name': 'Somalia'
	},{
	'value': 'ZA',
	'deliveryMethod': 'INT',
	'name': 'South Africa'
	},{
	'value': 'GS',
	'deliveryMethod': 'INT',
	'name': 'South Georgia and the South Sandwich Islands'
	},{
	'value': 'KR',
	'deliveryMethod': 'INT',
	'name': 'South Korea'
	},{
	'value': 'SS',
	'deliveryMethod': 'INT',
	'name': 'South Sudan'
	},{
	'value': 'ES',
	'deliveryMethod': 'EU',
	'name': 'Spain'
	},{
	'value': 'LK',
	'deliveryMethod': 'INT',
	'name': 'Sri Lanka'
	},{
	'value': 'PM',
	'deliveryMethod': 'INT',
	'name': 'St. Pierre and Miquelon'
	},{
	'value': 'SD',
	'deliveryMethod': 'INT',
	'name': 'Sudan'
	},{
	'value': 'SR',
	'deliveryMethod': 'INT',
	'name': 'Suriname'
	},{
	'value': 'SJ',
	'deliveryMethod': 'INT',
	'name': 'Svalbard and Jan Mayen Islands'
	},{
	'value': 'SZ',
	'deliveryMethod': 'INT',
	'name': 'Swaziland'
	},{
	'value': 'SE',
	'deliveryMethod': 'CAT2',
	'name': 'Sweden'
	},{
	'value': 'CH',
	'deliveryMethod': 'INT',
	'name': 'Switzerland'
	},{
	'value': 'SY',
	'deliveryMethod': 'INT',
	'name': 'Syria'
	},{
	'value': 'TW',
	'deliveryMethod': 'INT',
	'name': 'Taiwan'
	},{
	'value': 'TJ',
	'deliveryMethod': 'INT',
	'name': 'Tajikistan'
	},{
	'value': 'TZ',
	'deliveryMethod': 'INT',
	'name': 'Tanzania'
	},{
	'value': 'TH',
	'deliveryMethod': 'INT',
	'name': 'Thailand'
	},{
	'value': 'NL',
	'deliveryMethod': 'EU',
	'name': 'The Netherlands'
	},{
	'value': 'TL',
	'deliveryMethod': 'INT',
	'name': 'Timor-Leste'
	},{
	'value': 'TG',
	'deliveryMethod': 'INT',
	'name': 'Togo'
	},{
	'value': 'TK',
	'deliveryMethod': 'INT',
	'name': 'Tokelau'
	},{
	'value': 'TO',
	'deliveryMethod': 'INT',
	'name': 'Tonga'
	},{
	'value': 'TT',
	'deliveryMethod': 'INT',
	'name': 'Trinidad and Tobago'
	},{
	'value': 'TN',
	'deliveryMethod': 'INT',
	'name': 'Tunisia'
	},{
	'value': 'TR',
	'deliveryMethod': 'INT',
	'name': 'Turkey'
	},{
	'value': 'TM',
	'deliveryMethod': 'INT',
	'name': 'Turkmenistan'
	},{
	'value': 'TC',
	'deliveryMethod': 'INT',
	'name': 'Turks and Caicos Islands'
	},{
	'value': 'TV',
	'deliveryMethod': 'INT',
	'name': 'Tuvalu'
	},{
	'value': 'UG',
	'deliveryMethod': 'INT',
	'name': 'Uganda'
	},{
	'value': 'UA',
	'deliveryMethod': 'INT',
	'name': 'Ukraine'
	},{
	'value': 'AE',
	'deliveryMethod': 'INT',
	'name': 'United Arab Emirates'
	},{
	'value': 'UK',
	'deliveryMethod': 'UK',
	'name': 'United Kingdom'
	},{
	'value': 'US',
	'deliveryMethod': 'CAT1',
	'name': 'United States'
	},{
	'value': 'UM',
	'deliveryMethod': 'INT',
	'name': 'United States Minor Outlying Islands'
	},{
	'value': 'UY',
	'deliveryMethod': 'INT',
	'name': 'Uruguay'
	},{
	'value': 'UZ',
	'deliveryMethod': 'INT',
	'name': 'Uzbekistan'
	},{
	'value': 'VU',
	'deliveryMethod': 'INT',
	'name': 'Vanuatu'
	},{
	'value': 'VA',
	'deliveryMethod': 'INT',
	'name': 'Vatican'
	},{
	'value': 'VE',
	'deliveryMethod': 'INT',
	'name': 'Venezuela'
	},{
	'value': 'VN',
	'deliveryMethod': 'INT',
	'name': 'Vietnam'
	},{
	'value': 'VG',
	'deliveryMethod': 'INT',
	'name': 'Virgin Islands (British)'
	},{
	'value': 'VI',
	'deliveryMethod': 'INT',
	'name': 'Virgin Islands (U.S.)'
	},{
	'value': 'WF',
	'deliveryMethod': 'INT',
	'name': 'Wallis and Futuna Islands'
	},{
	'value': 'EH',
	'deliveryMethod': 'INT',
	'name': 'Western Sahara'
	},{
	'value': 'YE',
	'deliveryMethod': 'INT',
	'name': 'Yemen'
	},{
	'value': 'ZM',
	'deliveryMethod': 'INT',
	'name': 'Zambia'
	},{
	'value': 'ZW',
	'deliveryMethod': 'INT',
	'name': 'Zimbabwe'
	},{
	'value': 'AX',
	'deliveryMethod': 'INT',
	'name': 'Åland Islands'
	}
]

export const titles = [
    { "label": "Mr.", "value": "mr" },
    { "label": "Ms.", "value": "ms" },
    { "label": "Miss", "value": "miss" },
    { "label": "Mrs.", "value": "mrs" },
    { "label": "Dr.", "value": "dr" },
    { "label": "Mx.", "value": "mx" },
    { "label": "Rev.", "value": "rev" }
]

export const usaStates = [
	{
	'value':'US-AL',
	'name': 'Alabama'
	},{
	'value':'US-AK',
	'name': 'Alaska'
	},{
	'value':'US-AS',
	'name': 'American Samoa'
	},{
	'value':'US-AZ',
	'name': 'Arizona'
	},{
	'value':'US-AR',
	'name': 'Arkansas'
	},{
	'value':'US-CA',
	'name': 'California'
	},{
	'value':'US-CO',
	'name': 'Colorado'
	},{
	'value':'US-CT',
	'name': 'Connecticut'
	},{
	'value':'US-DE',
	'name': 'Delaware'
	},{
	'value':'US-DC',
	'name': 'District of Columbia'
	},{
	'value':'US-FL',
	'name': 'Florida'
	},{
	'value':'US-GA',
	'name': 'Georgia'
	},{
	'value':'US-GU',
	'name': 'Guam'
	},{
	'value':'US-HI',
	'name': 'Hawaii'
	},{
	'value':'US-ID',
	'name': 'Idaho'
	},{
	'value':'US-IL',
	'name': 'Illinois'
	},{
	'value':'US-IN',
	'name': 'Indiana'
	},{
	'value':'US-IA',
	'name': 'Iowa'
	},{
	'value':'US-KS',
	'name': 'Kansas'
	},{
	'value':'US-KY',
	'name': 'Kentucky'
	},{
	'value':'US-LA',
	'name': 'Louisiana'
	},{
	'value':'US-ME',
	'name': 'Maine'
	},{
	'value':'US-MD',
	'name': 'Maryland'
	},{
	'value':'US-MA',
	'name': 'Massachusetts'
	},{
	'value':'US-MI',
	'name': 'Michigan'
	},{
	'value':'US-MN',
	'name': 'Minnesota'
	},{
	'value':'US-MS',
	'name': 'Mississippi'
	},{
	'value':'US-MO',
	'name': 'Missouri'
	},{
	'value':'US-MT',
	'name': 'Montana'
	},{
	'value':'US-NE',
	'name': 'Nebraska'
	},{
	'value':'US-NV',
	'name': 'Nevada'
	},{
	'value':'US-NH',
	'name': 'New Hampshire'
	},{
	'value':'US-NJ',
	'name': 'New Jersey'
	},{
	'value':'US-NM',
	'name': 'New Mexico'
	},{
	'value':'US-NY',
	'name': 'New York'
	},{
	'value':'US-NC',
	'name': 'North Carolina'
	},{
	'value':'US-ND',
	'name': 'North Dakota'
	},{
	'value':'US-MP',
	'name': 'Northern Mariana Islands'
	},{
	'value':'US-OH',
	'name': 'Ohio'
	},{
	'value':'US-OK',
	'name': 'Oklahoma'
	},{
	'value':'US-OR',
	'name': 'Oregon'
	},{
	'value':'US-PA',
	'name': 'Pennsylvania'
	},{
	'value':'US-PR',
	'name': 'Puerto Rico'
	},{
	'value':'US-RI',
	'name': 'Rhode Island'
	},{
	'value':'US-SC',
	'name': 'South Carolina'
	},{
	'value':'US-SD',
	'name': 'South Dakota'
	},{
	'value':'US-TN',
	'name': 'Tennessee'
	},{
	'value':'US-TX',
	'name': 'Texas'
	},{
	'value':'US-UM',
	'name': 'United States Minor Outlying Islands'
	},{
	'value':'US-VI',
	'name': 'United States Virgin Islands'
	},{
	'value':'US-UT',
	'name': 'Utah'
	},{
	'value':'US-VT',
	'name': 'Vermont'
	},{
	'value':'US-VA',
	'name': 'Virginia'
	},{
	'value':'US-WA',
	'name': 'Washington'
	},{
	'value':'US-WV',
	'name': 'West Virginia'
	},{
	'value':'US-WI',
	'name': 'Wisconsin'
	},{
	'value':'US-WY',
	'name': 'Wyoming'}
]