import React from 'react';

const AppContext = React.createContext();
const AppConsumer = AppContext.Consumer;

class AppProvider extends React.Component {
  state = {
    modalOpened: false,
    authUser: false,

    deliveryCountry: '',
    deliveryCountryType: '',
    
    // Collection
    collectionPointOfServices: '',
    collectionPostcodeSelected: '',
    collectionConfirmedStore: '',
    collectionCollectorDetails: '',

    // Home delivery
    deliveryMethodParent: [],
    deliveryMethodParentSelected: '',
    deliveryMethodChild: [],
    deliveryMethodChildSelected: '',
    deliveryWorldwideType: '',
    deliveryAddress: [],
    deliveryHasAddress: false,
    deliveryInstructions: '',
    deliveryNominated: {
      day: '',
      time: '',
      cost: ''
    }

  };

  updateModalVisibility = value => {
    this.setState({ modalOpened: value });
  };

  updateDeliveryCountry = (country, type) => {
    console.log("type", type);
    console.log("country", country);
    this.setState({ deliveryCountry: country, deliveryCountryType: type });
  };

  updatedeliveryMethodParent = method => {
    this.setState({ deliveryMethodParent: method });
  };

  updatedeliveryMethodChild = method => {
    this.setState({ deliveryMethodChild: method });
  };

  updatedeliveryMethodParentSelected = method => {
    this.setState({ deliveryMethodParentSelected: method });
  };

  updateDeliveryMethodChildSelected = option => {
    this.setState({ deliveryMethodChildSelected: option });
  };

  updateDeliveryAddress = address => {
    this.setState({ deliveryAddress: address });
    address.length > 0
      ? this.setState({ deliveryHasAddress: true })
      : this.setState({ deliveryHasAddress: false });
  };

  updateDeliveryInstructions = option => {
    this.setState({ deliveryInstructions: option });
  };

  updateCollectionStores = (postcode, stores) => {
    this.setState({ collectionPointOfServices: stores, collectionPostcodeSelected: postcode });
  };

  updateConfirmedSelectedStore = store => {
    this.setState({ collectionConfirmedStore: store });
  };

  updatecollectionCollectorDetails = store => {
    this.setState({ collectionCollectorDetails: store });
  };

  updateNominatedSelection = (day, time, cost) => {
    this.setState({ deliveryNominated: {day:day, time:time, cost:cost} });
  };


  componentWillMount() {
    // Country Select
    let countrySelected = JSON.parse(localStorage.getItem('countrySelected')) || '';
    let deliveryCountryType = JSON.parse(localStorage.getItem('deliveryCountryType')) || '';
    this.updateDeliveryCountry(countrySelected, deliveryCountryType, true);
  
    // Parent delivery method
    let deliveryMethodSelect = JSON.parse(localStorage.getItem('deliveryMethodSelect')) || '';
    this.updatedeliveryMethodParentSelected(deliveryMethodSelect, true);

    // Delivery Home Details
    if (countrySelected && deliveryMethodSelect) {

      // Delivery Option
      let deliveryMethodChildSelected = JSON.parse(localStorage.getItem('deliveryMethodChildSelected')) || '';
      this.updateDeliveryMethodChildSelected(deliveryMethodChildSelected, true);
    }
  
    // Confirmed Home Address
    let addressSelected = JSON.parse(localStorage.getItem('addressSelected')) || '';
    if (addressSelected) {
      this.updateDeliveryAddress(addressSelected, true);
    }
    
    // Update delivery instructions
    let deliveryInstructions = JSON.parse(localStorage.getItem('deliveryInstructions')) || '';
    this.updateDeliveryInstructions(deliveryInstructions, true);

    // Nominated day
    let deliveryNominated = JSON.parse(localStorage.getItem('deliveryNominated')) || '';
    this.updateNominatedSelection(deliveryNominated.day, deliveryNominated.time, deliveryNominated.cost, true);

  }

  render() {
    return (
      <AppContext.Provider
        value={{
          state: this.state,
          updateModalVisibility: this.updateModalVisibility,
          updateDeliveryCountry: this.updateDeliveryCountry,
          updatedeliveryMethodParent: this.updatedeliveryMethodParent,
          updatedeliveryMethodChild: this.updatedeliveryMethodChild,
          updatedeliveryMethodParentSelected: this.updatedeliveryMethodParentSelected,
          updateDeliveryMethodChildSelected: this.updateDeliveryMethodChildSelected,
          updateDeliveryAddress: this.updateDeliveryAddress,
          updateDeliveryInstructions: this.updateDeliveryInstructions,
          updateCollectionStores: this.updateCollectionStores,
          updateConfirmedSelectedStore: this.updateConfirmedSelectedStore,
          updatecollectionCollectorDetails: this.updatecollectionCollectorDetails,
          updateNominatedSelection: this.updateNominatedSelection
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export { AppProvider, AppConsumer };
