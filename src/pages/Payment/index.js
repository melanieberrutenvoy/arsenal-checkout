import React, { Component } from 'react';
import CheckoutSteps from '../../components/CheckoutSteps';
import { AppConsumer } from '../../AppContext';
import endpoints from '../../endpoints';
import axios from 'axios';

class Payment extends Component {
  constructor() {
    super();

    this.state = {
      paymentMethods: [],
      paymentMethodSelected: ''
    };

    this.handlePaymentMethodSelection = this.handlePaymentMethodSelection.bind(this);
  }

  componentWillMount() {
    // Fetch the payment method avail
    axios.get(endpoints.payment).then(response => {
      let paymentMethods = response.data;
      this.setState({ paymentMethods });
    });
  }

  handlePaymentMethodSelection(e) {
    let paymentMethodSelected = e.target.value;
    this.setState({ paymentMethodSelected: paymentMethodSelected });
  }

  render() {
    return (
      <div className="payment">
        <CheckoutSteps activeStep={2} />
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4">
              <h1 className="checkout-heading">Choose payment</h1>

              {this.state.paymentMethods.map((type, i) => (
                <div key={i} className="form-group form-group--radio-selection">
                  <div className="radio-selection">
                    <label htmlFor={'group' + type.id} className="radio-selection__label">
                      {type.label}
                    </label>
                    <input
                      className="radio-selection__radio"
                      type="radio"
                      name="paymentMethod"
                      id={'group' + type.id}
                      value={type.id}
                      selected={this.state.paymentMethodSelected === type.id}
                      onChange={this.handlePaymentMethodSelection}
                    />
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default props => (
  <AppConsumer>{context => <Payment {...props} context={context} />}</AppConsumer>
);
