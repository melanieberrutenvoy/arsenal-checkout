import React, { Component } from 'react';
import { AppConsumer } from '../../AppContext';
import CheckoutSteps from '../../components/CheckoutSteps';

import ChooseCountryAndDeliveryMethod from '../../components/ChooseCountryAndDeliveryMethod';
import DeliverySubType from '../../components/DeliverySubType';

import AddressFormValidation from '../../components/AddressFormValidation';
import ClickCollect from '../../components/ClickCollect';
import ClickCollectUser from '../../components/ClickCollectUser';

import endpoints from '../../endpoints';
import axios from 'axios';

class Delivery extends Component {
  constructor() {
    super();

    this.state = {
      active: '',
      deliveryInstructions: '',
      collectionPointOfServices: [],
      countrySelected: ''
    };

    this.createChildDeliveryOptionFromParent = this.createChildDeliveryOptionFromParent.bind(this);
    this.confirmTheAddress = this.confirmTheAddress.bind(this);
  }

  componentDidMount() {
    let context = this.props.context.state;

    // // Country Selected
    // let countrySelected = context.deliveryCountry;

    // // Show the delivery parent option
    // if(countrySelected.length > 0) {this.fetchParentDeliveryCategories(countrySelected); }

    // Delivery Method Selected
    let deliveryMethodSelect = context.deliveryMethodParentSelected;

    // Delivery options
    // if (countrySelected && deliveryMethodSelect) {
    //   this.createChildDeliveryOptionFromParent(deliveryMethodSelect);
    // }

    if (this.props.location.hash === '#1') {
      this.setState({ active: 1 });
    } else if (this.props.location.hash === '#2' || this.props.location.hash === '#2b') {
      this.setState({ active: 2 });
    } else if (this.props.location.hash === '#3') {
      this.setState({ active: 3 });
    }
  }

  componentWillUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      var historyStep = this.props.history.location.hash;
      if (historyStep === '#1') {
        this.setState({ active: 1 });
      } else if (historyStep === '#2' || historyStep === '#2b') {
        this.setState({ active: 2 });
      } else if (historyStep === '#3') {
        this.setState({ active: 3 });
      }
    }
  }

  createChildDeliveryOptionFromParent(deliveryMethodSelected) {
    let deliveryParentOptions = JSON.parse(localStorage.getItem('parentDeliveryCategories')) || '';
    let deliveryOptions = deliveryParentOptions.filter(parent => parent.id === deliveryMethodSelected);

    if (deliveryOptions.length > 0) {
      deliveryOptions = deliveryOptions[0].options;
    }
    this.props.context.updatedeliveryMethodChild(deliveryOptions, true);
  }

  fetchParentDeliveryCategories(countrySelected, deliveryCountryType) {
    // Fetch the associated delivery parents categories
    let context = this.props.context.state;

    axios.get(endpoints.delivery).then(response => {
      let parentDeliveryCategories;

      if (deliveryCountryType === 'UK') {
        // console.log('deliveryCountryType === UK')
        parentDeliveryCategories = response.data.uk;

      } else if (deliveryCountryType === 'EU') {
        // console.log('deliveryCountryType === EU')
        parentDeliveryCategories = response.data.eu;

      } else if (deliveryCountryType === 'INT') {
        // console.log('deliveryCountryType === INT')
        parentDeliveryCategories = response.data.wordlwide;

      } else if (deliveryCountryType === 'CAT1') {
        // console.log('deliveryCountryType === CAT1')
        if(countrySelected === 'IE') { 
          parentDeliveryCategories = response.data.ie;
        } else {
           parentDeliveryCategories = response.data.wordlwide;
        }

      } else if (deliveryCountryType === 'CAT2') {
        // console.log('deliveryCountryType === CAT2')
        parentDeliveryCategories = response.data.eu;
      }
      
      // Store the delivery method
      this.props.context.updatedeliveryMethodParent(parentDeliveryCategories, true);
      localStorage.setItem('parentDeliveryCategories', JSON.stringify(parentDeliveryCategories));
    });
  }

  confirmTheAddress() {
    this.props.history.replace('delivery#1');
    this.props.history.push('/delivery#3');
  }

  render() {
    let context = this.props.context.state;

    return (
      <div className="delivery">
        <CheckoutSteps activeStep={1} />
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4">
              {/* START:  STEP 1: Country + Delivery type */}

              {this.state.active === 1 && (
                <ChooseCountryAndDeliveryMethod
                  history={this.props.history}
                  fetchParentDeliveryCategories={(countrySelected, deliveryCountryType)=> this.fetchParentDeliveryCategories(countrySelected, deliveryCountryType)}
                  createChildDeliveryOptionFromParent={deliveryMethodSelected =>
                    this.createChildDeliveryOptionFromParent(deliveryMethodSelected)
                  }
                />
              )}
              {/* END:  STEP 1: Country + Delivery type */}

              {/* START:  STEP 2: UK DELIVERY - Delivery address */}
              {this.state.active === 2 &&
                context.deliveryMethodParentSelected === 'delivery' && (
                  <React.Fragment>
                    <AddressFormValidation
                      firstStepTitle="Deliver To"
                      secondStepTitle="Select an address"
                      confirmTheAddress={this.confirmTheAddress}
                    />
                  </React.Fragment>
                )}
              {/* END:  STEP 2: UK DELIVERY - Delivery address */}

              {/* START:  STEP 2: UK DELIVERY - Delivery address */}
              {this.state.active === 2 &&
                context.deliveryMethodParentSelected === 'wordlwide' && (
                  <React.Fragment>
                    <AddressFormValidation
                      firstStepTitle="Deliver To"
                      secondStepTitle="Select an address"
                      confirmTheAddress={this.confirmTheAddress}
                    />
                  </React.Fragment>
                )}
              {/* END:  STEP 2: UK DELIVERY - Delivery address */}

              {/* START:  STEP 2: COLLECT TO STORE */}
              {this.state.active === 2 &&
                context.deliveryMethodParentSelected === 'collect' && (
                  <React.Fragment>
                    {context.collectionConfirmedStore.length === 0 && (
                      <ClickCollect
                        storesQty={this.state.collectionPointOfServices.length}
                        stores={this.state.collectionPointOfServices}
                        history={this.props.history}
                      />
                    )}
                    {context.collectionConfirmedStore.length > 0 && (
                      <div className="">
                        <h1 className="checkout-heading">Who will be collecting the order?</h1>
                        <ClickCollectUser history={this.props.history} />
                      </div>
                    )}
                  </React.Fragment>
                )}
              {/* END:    STEP 2: COLLECT TO STORE */}

              {/* START:  STEP 3: Delivery subtypes */}
              {this.state.active === 3 && <DeliverySubType history={this.props.history} />}

              {/* END:  STEP 3: Delivery subtypes */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default props => <AppConsumer>{context => <Delivery {...props} context={context} />}</AppConsumer>;