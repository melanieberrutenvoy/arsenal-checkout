import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class SignIn extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      password: ''
    };

    this.handleEmail = this.handleEmail.bind(this);
    this.handlePwd = this.handlePwd.bind(this);
  }

  handleEmail(e, key) {
    this.setState({ email: e.target.value });
  }

  handlePwd(e, key) {
    this.setState({ password: e.target.value });
  }

  render() {
    let disableBtn = this.state.email === '' || this.state.password === '';

    return (
      <div className="welcome col-xs-12">
        <div className="row">
          <div className="col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4 text-center-md">
            <h1 className="text-center">Secure checkout</h1>
            <h2>Please enter your email to start</h2>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4 text-center-md mb20">
            <ul className="bg-warning text-left pt20 pb20 mb20">
              <li>We'll use your email to confirm your order</li>

              <li>If you already have an account, we'&#39;'ll ask you for a password next</li>
            </ul>

            <div className="form-group">
              <div className="form-group__label">
                <label htmlFor="exampleInputEmail1" className="">
                  Email Address
                </label>
              </div>
              <div className="form-group__input">
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputEmail1"
                  placeholder=""
                  onChange={this.handleEmail}
                />
              </div>
            </div>
            {this.state.email.length > 0 && (
              <div className="form-group">
                <div className="form-group__label">
                  <label htmlFor="exampleInputPwd1" className="">
                    Password
                  </label>
                </div>
                <div className="form-group__input">
                  <input
                    type="text"
                    className="form-control"
                    id="exampleInputPwd1"
                    placeholder=""
                    onChange={this.handlePwd}
                  />
                </div>
              </div>
            )}

            <NavLink className="btn btn-primary btn-block" to="/delivery" disabled={disableBtn}>
              Continue
            </NavLink>
          </div>
        </div>
      </div>
    );
  }
}

export default SignIn;
