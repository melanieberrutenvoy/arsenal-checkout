import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Welcome extends Component {
  render() {
    return (
      <div className="container">
        <div className="welcome col-xs-12">
          <div className="row">
            <div className="col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4 text-center-md">
              <h1 className="checkout-page-header checkout-page-header--intro text-center">Secure checkout</h1>
            </div>
          </div>

          <div className="row">
            <div className="form form--md-center col-xs-12 col-sm-8 col-md-4 col-sm-push-2 col-md-push-4 text-center-md">
              <div className="form-group form-group--help">
                <div className="form-group__label form-group__label--intro">
                  New to Arsenal Store
                </div>
                <div className="form-group__link">
                  <NavLink className="btn btn-primary btn-block" to="/delivery#1">
                    Checkout as guest
                  </NavLink>
                </div>
              </div>

              <span className="help-block ">You can sign up for an account later</span>

              <hr className="hr-text" data-content="OR" />

              <div className="form-group form-group--help">
                <div className="form-group__label form-group__label--intro">Existing customer</div>
                <div className="form-group__link">
                  <NavLink className="btn btn-secondary btn-block" to="/signin">
                    Sign In
                  </NavLink>
                </div>
              </div>

              <span className="help-block ">
                <NavLink to="/forgot-password">Forgot your password ?</NavLink>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Welcome;
