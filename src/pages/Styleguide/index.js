import React, { Component } from 'react';
class Styleguide extends Component {
  render() {
    return (
      <div className="styleguide">
        <p className="test-meta-normal">Lorem test-meta-normal ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-meta-bold">Lorem test-meta-bold ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-meta">Lorem test-meta">Lorem ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-meta-medium">Lorem test-meta-medium ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-proxima-nova">Lorem test-proxima-nova ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <br/>
        <p className="test-meta-bold">Lorem test-meta-bold ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-meta-normal"><strong>Lorem test-meta-normal strong ipsum dolor sit amet, consectetur adipisicing elit 12345678.</strong></p>
        <br/>

        <p className="test-large test-meta-normal">Lorem test-meta-normal ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-large test-meta-bold">Lorem test-meta-bold ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-large test-meta">Lorem ipsum dtest-meta">Lorem olor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-large test-meta-medium">Lorem test-meta-medium ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <p className="test-large test-proxima-nova">Lorem test-proxima-nova ipsum dolor sit amet, consectetur adipisicing elit 12345678.</p>
        <br/><br/><br/>
        <hr/>
        <br/>
        <button className="btn-red-primary">btn-red-primary</button>
        <button className="btn--secondary">btn--secondary</button>
        <button className="btn--red">btn--red</button>
        <button className="btn--red">btn--gray</button>
        <button className="btn-grey-gradient">btn-grey-gradient</button>

        <br />

        <button className="btn-primary">btn-primary</button>
        <button className="btn-secondary">btn-secondary</button>
        <br />

        <button className="btn btn-ghost-primary btn-lg">btn-ghost-primary</button>
        <button className="btn btn-ghost-primary">btn-ghost-primary</button>
        <button className="btn btn-ghost-primary btn-sm">btn-ghost-primary</button>
        <button className="btn btn-ghost-primary btn-xs">btn-ghost-primary</button>

        <br />
        <br />
      </div>
    );
  }
}

export default Styleguide;
